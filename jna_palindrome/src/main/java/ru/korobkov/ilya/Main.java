package ru.korobkov.ilya;

import java.util.Scanner;
public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input string for 'palindrome' check: ");
		String str = scanner.nextLine();
		if (str.isEmpty()) {
			System.out.println("Empty string. Please re-start the program and input non-empty string");
			return;
		}
		MyClass myClass = new MyClass(str);
		System.out.printf("Result: %s%n", myClass.isValid() ? "true, palindrome" : "false, not palindrome");
	}
	
}
