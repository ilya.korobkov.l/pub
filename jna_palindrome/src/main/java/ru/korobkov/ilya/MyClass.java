package ru.korobkov.ilya;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;

public class MyClass {

    public interface CLibrary extends Library {
        String dllPath = System.getProperty("user.dir") + "\\src\\main\\cpp\\dll\\Release\\";
        CLibrary INSTANCE = (CLibrary) Native.loadLibrary(dllPath + "cpplib_shared.dll", CLibrary.class);
        //CLibrary INSTANCE = (CLibrary)Native.load("cpplib_shared", CLibrary.class);

        Pointer MyClass_ctor(String str);

        boolean MyClass_isValid(Pointer self);
    }

    private Pointer self;

    public MyClass(String str)
    {
        self = CLibrary.INSTANCE.MyClass_ctor(str);
    }

    public boolean isValid()
    {
        return CLibrary.INSTANCE.MyClass_isValid(self);
    }

}
