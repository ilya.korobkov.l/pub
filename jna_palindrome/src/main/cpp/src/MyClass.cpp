#include "MyClass.h"

MyClass::MyClass(const char* str): m_str(str)
{    }

bool MyClass::isValid() const {
    string s = m_str;
    if (s.empty()) {
        return false;
    }
    int start = 0;
    int finish = (int) s.size() - 1;
    while (start <= finish) {
        if (!isalnum(s[start])) {
            start++;
            continue;
        }
        if (!isalnum(s[finish])) {
            finish--; 
            continue;
        }
        if (tolower(s[start]) != tolower(s[finish])) {
            return false;
        }
        else {
            start++;
            finish--;
        }
    }
    return true;
}
