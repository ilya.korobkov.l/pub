#ifndef MYCLASS_H
#define MYCLASS_H

#include "string"
using namespace std;

class MyClass {

private:
    string m_str;

public:
    MyClass(const char* st);
    bool isValid() const;
};

#endif
