#include "MyClass.h"

#ifdef _WIN32
#   define DLL_EXPORT __declspec(dllexport)
#else
#   define DLL_EXPORT
#endif

extern "C" DLL_EXPORT MyClass* MyClass_ctor(const char* str)
{
    return new MyClass(str);
}

extern "C" DLL_EXPORT bool MyClass_isValid(const MyClass* self)
{
    return self->isValid();
}
