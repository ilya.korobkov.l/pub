# Пример JAVA JNA и С++
### Постановка задачи
Написать на Java приложение, которое будет использовать
реализацию функции на С++ (используя JNA). Это приложение/функция должна будет проверять
переданную строку является ли она палиндромом.
Пример палиндрома:

> "Madam, I'm Adam"

### Установка и запуск программы
1. Cклонируйте репозиторий проекта.

2. Скомпилируйте С++ DLL **cpplib_shared**:

   - Шаг 1. Установите CMake GUI (Windows): https://cmake.org/download/

   - Шаг 2. Установите C++ компилятор/среду разработки. Например, IDE VS2022 Community Edition: https://visualstudio.microsoft.com/ru/vs/community/

   - Шаг 3. CMake, генерация файлов С++ **cpplib_shared**.

     Перейдите в корневой каталог проекта **jna_palindrome**. Запустите CMake GUI. Укажите папку с исходниками **\jna_palindrome\src\main\cpp** и папку куда выгрузить бинарники **\jna_palindrome\src\main\cpp\src**. Укажите под какую среду разработки/компилятор сформировать файлы проекта. Например, укажите Visual Studio 2022. Нажмите **Configure**. Дождитесь завершения и затем нажмите **Generate**.

     ![Рис.1. Пример CMake GUI](img/1.png)

     Рисунок 1. Пример CMake GUI

     

     (эти же действия можно выполнить через терминал, набрав:)

     - cd .\src\main\cpp\dll
     - cmake ../

     

   - Шаг 4. Компиляция C++ **cpplib_shared** и получение DLL (Windows).
     
     В среде С++ Visual Studio скомпилируйте код и получите в папке
     
     - \jna_palindrome\src\main\cpp\dll\Release\\**cpplib_shared.lib**
     - \jna_palindrome\src\main\cpp\dll\Release\\**cpplib_shared.dll**

3. Создайте JAVA проект и подключите JNA

     - Шаг 1. Установите JAVA SDK (не ниже 11 версии) и среду разработки IDE Intelij IDEA Community Edition: https://www.jetbrains.com/idea/download/

     - Шаг 2. Cоздайте JAVA проект из существующих исходников, указав каталог jna_palindrome.

       - Замечание. 

         Если создать проект из существующих исходников не получается или по какой-либо причине Intelij IDEA  открывает с ошибками текущий проект, то сздайте пустой проект c названием  **jna_palindrome** и системой сборки **Maven**.

         Добавьте JNA, указав в pom.xml следующие строки:   

       ```
       <dependencies>
           <dependency>
               <groupId>net.java.dev.jna</groupId>
               <artifactId>jna</artifactId>
               <version>5.13.0</version>
           </dependency>
       </dependencies>
       ```

       ​	Добавьте в проект свой package. Например, ru.korobkov.ilya.

       ​	Добавьте файлы исходников JAVA программы из каталога **\jna_palindrome\src\main\java\ru\korobkov\ilya\ **в Ваш проект:

       - MyClass.java

       - Main.java

         Проверьте иерархию Вашего каталога. Должно получиться:

         - \jna_palindrome\pom.xml
         - \jna_palindrome\src\main\java\ package \MyClass.java
         - \jna_palindrome\src\main\java\ package \Main.java
         - \jna_palindrome\src\main\cpp\CMakeLists.txt
         - \jna_palindrome\src\main\cpp\src\MyClass.h
         - \jna_palindrome\src\main\cpp\src\MyClass.cpp
         - \jna_palindrome\src\main\cpp\src\c_wrapper.cpp
         - \jna_palindrome\src\main\cpp\dll\Release\cpplib_shared.lib
         - \jna_palindrome\src\main\cpp\dll\Release\cpplib_shared.dll

4. Скомпилируйте и запустите Intelij IDEA.

5. Введите **непустую** строку (латиницей) на проверку является ли палиндромом предложенная строка. Нажмите Enter. Дождитесь результата выполнения программы.

     ![Рис.2. Результат выполнения программы](img/2.png)

     Рисунок 2. Результат выполнения программы









